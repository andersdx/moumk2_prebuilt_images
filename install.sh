#!/bin/bash

# command > /dev/null			does not display output
# command > /dev/null 2>&1      	ignores errors
# \e[32m						for GREEN text
# \e[39m						for WHITE default text
# \e[34m						for BLUE text
# \e[5m							for BLINKING text
# \e[25m							stop blinking
# \e[1m							for BOLD text
# \e[0m								for default text

ANNOUNCE () {
	echo -e "\e[1;32m${1} \e[0;39m"
}

WAIT() {
	echo -en "\e[32m${1}\e[5m...\e[25;39m"
}

DONE() {
	echo -e "\b\b\b\e[1;32m: DONE!\e[0;39m"
}

echo
ANNOUNCE "========== FLASHING PROCESS WILL NOW COMMENCE! =========="
echo

# Kernel write
ANNOUNCE "Preparing kernel: "

WAIT "Creating kernel partition"
flash_erase /dev/mtd5 0 0 > /dev/null &> /dev/null
sleep 5
DONE

WAIT "Writing kernel image to partition"
nandwrite -p /dev/mtd5 /media/mmcblk0p1/uImage &> /dev/null
DONE
echo

# Rootfs write
ANNOUNCE "Preparing root filesystem: "

WAIT "Creating rootfs partition"
flash_erase /dev/mtd6 0 0 &> /dev/null
sleep 5
DONE

WAIT "Formating rootfs partition"
ubiformat /dev/mtd6 &> /dev/null
sleep 2
DONE

WAIT "Mounting rootfs"
ubiattach -m 6 -d 0 &> /dev/null
sleep 2
ubimkvol /dev/ubi0 -m -N rootfs &> /dev/null
mkdir -p /media/rootfs && mount -t ubifs ubi0:rootfs /media/rootfs &> /dev/null
sleep 2
DONE

echo -e "\e[32mExtracting root filesystem:\e[39m"
pv /media/mmcblk0p1/moumk2-image.tar.bz2 | tar --numeric-owner -xpjf - -C /media/rootfs > /dev/null && sync
sleep 2
umount /media/rootfs > /dev/null
echo -e "\e[32mRoot filesystem installed.\e[39m"

# Closing sequence
echo
echo
ANNOUNCE "========================================================="
echo
echo
ANNOUNCE "YOUR DEVICE IS NOW READY! "
echo
ANNOUNCE "PLEASE REMOVE INSTALLATION SD CARD AND REBOOT THE DEVICE."
echo
echo
ANNOUNCE "========================================================="
echo

exit 0
